'use strict';

/**
 * @ngdoc function
 * @name angulartestApp.controller:HomeCtrl
 * @description
 * # HomeCtrl
 * Controller of the angulartestApp
 */
angular.module('angulartestApp')
  .controller('HomeCtrl', ['$scope', function () {
    
    // Showing active page on nav menu
    $('nav li').removeClass('active');
    $('nav li:first-child').addClass('active');

  }]);
