'use strict';

var $ = window.jQuery; // to avoid any possible conflict and make jshint happier

/**0
 * @ngdoc function
 * @name angulartestApp.controller:GameCtrl
 * @description
 * # GameCtrl
 * Controller of the angulartestApp
 */
angular.module('angulartestApp')
  // main Game controller
  .controller('GameCtrl', ['$scope', '$http', function ($scope, $http) {

    // Declaring map variables
    var map, layerProvider, baseLayer;

    // Showing active page on nav menu
    $('nav li').removeClass('active');
    $('nav li:last-child').addClass('active');
    
    // Loading data and running the app
    $http.get('/data/cities.json').then(function(res) {
      // call the main game function passing it the data from the response
      playGame(res.data);

    }, function(error) {
      console.log(error.status + " " + error.data);
    });

    // This game will use sessionStorage object to save its state
    // reset sessionStorage score and currentCity
    sessionStorage.clear();
    sessionStorage.setItem('yg_score', '1500'); 
    sessionStorage.setItem('yg_count', '0'); // used to know which city we're asking about
    sessionStorage.setItem('yg_correct', '0'); // used to keep track of correct answers

    // SETTING UP THE MAP
    map = new L.map('map', {
      center: [52, 7.5],
      maxZoom: 5,
      zoom: 3
    }); // injects the map into the DOM

    // create a new layer provider and tileLayer to use
    layerProvider = new L.tileLayer.provider('Esri.WorldGrayCanvas').addTo(map);
    baseLayer = new L.tileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/Canvas/World_Light_Gray_Base/MapServer/tile/{z}/{y}/{x}', { 
      attribution: 'Tiles &copy; Esri &mdash; Esri, DeLorme, NAVTEQ'
    });

    /* 
     * The game function
     * @params 'data' generated from $http response
     */
    function playGame(data) {

      // Initialising variables
      var marker = null, circle = null, capitalsArray = data.capitalCities;

      // capture the user click on map and deal with it
      map.on('click', function(e) {
        var count = sessionStorage.getItem('yg_count'), distance;

        // Delete any previous markers
        if (map.hasLayer(marker)) {
          map.removeLayer(marker);
        }

        // grab the coordinates of the answer
        $scope.latlng = [capitalsArray[count].lat, capitalsArray[count].long];

        // Calculate how far is the user's marker from the city
        distance = e.latlng.distanceTo($scope.latlng);

        // create and place the new marker
        marker = new L.Marker(e.latlng).addTo(map);

        // set $scope.result to true or false according to error
        // set sessionStorage key|value to store error distance in meters
        if (distance <= 50000) {
          $scope.result = true;
        } else {
          $scope.result = false;
          sessionStorage.setItem('yg_error', Math.round(distance/1000));
        }

        // activate the button to submit answer 
        $('#confirm').removeClass('disabled');

      });

      /* 
       * Method checkAnswer
       * Deals with the answer and all actions derived
       * @params 'result' and 'latlng'
       */
      $scope.checkAnswer = function (result, latlng) {

        // a variety of feedback messages so as to spice them up a bit
        var messages = {
          "succes": [
            "You got that right, dude!",
            "Amaaazing!",
            "That's right!",
            "You should do this professionally!",
            "mmmmmm, I'm sorry, but that's.... Correct! :P"
          ],
          "fail": [
            "Uh, uh... someone was distracted during geography lessons.",
            "Getting colder...",
            "Not quite, mate.",
            "Unlucky!",
            "Would you like to ask the audience?"
          ]
        };

        // get sessionStorage counter and increment it by 1,
        // grab next city in a variable
        // grab number of correct answers in a variable
        var sessionCountPlus = parseInt(sessionStorage.getItem('yg_count')) + 1,
            nextCity = (sessionCountPlus <= 8) ? capitalsArray[sessionCountPlus].capitalCity : '',
            currentCorrectAnswers = parseInt(sessionStorage.getItem('yg_correct'));

        // show where the correct position of the city is
        // drawing a circle
        if (map.hasLayer(circle)) {
          map.removeLayer(circle);
        }
        circle = new L.circle(latlng, 50000, {color: "red"}).addTo(map);

        // CORRECT ANSWER
        if (result !== null && result === true) {
          // Show the user they got it right
          // disable the confirm button
          $('.answer').html(messages.succes[Math.floor(Math.random()*5)]);
          $('#confirm').addClass('disabled');

          // Enable the continue button
          // Actions to take on click on the continue button
          // Reset the map
          $('#continue').removeClass('disabled').click(function() {

            // Update sessionStorage
            // increase the counter
            // add one to the counter of correct answers
            sessionStorage.setItem('yg_count', sessionCountPlus);
            sessionStorage.setItem('yg_correct', currentCorrectAnswers + 1);

            // check if this was the last city
            checkIfEnd();

            // add new city to the DOM
            $('.city').html(nextCity);

            // reset interface 
            map.removeLayer(marker);
            map.removeLayer(circle);
            $('#continue').addClass('disabled');
            $('.answer').html('Your answer shows in here');
          });

        }

        // WITH INCORRECT ANSWER
        else {
          // get current values for score and error
          var currentScore = sessionStorage.getItem('yg_score'),
              errorCommitted = sessionStorage.getItem('yg_error'),
              newScore = currentScore - errorCommitted;
          
          // show red rim of shame
          $('.map-container').addClass('error');

          // KEEP playing as there are still some points left
          if (newScore > 0) {
            // update score on sessionStorage
            sessionStorage.setItem('yg_score', newScore);

            // tell the user how far they went
            // disable the confirm button
            $('.answer').html(messages.fail[Math.floor(Math.random()*5)] + " You were off by " + errorCommitted + "km");
            $('#confirm').addClass('disabled');
            
            // Enable the continue button and 
            // RESET the map for the next city
            $('#continue').removeClass('disabled').click(function() {

              // take away red rim on map
              $('.map-container').removeClass('error');

              // increase the counter
              sessionStorage.setItem('yg_count', sessionCountPlus);

              // check if this was the last city
              checkIfEnd();

              // set new city to the DOM
              // update score on page
              $('.city').html(nextCity);
              $('.score').html(newScore);
            
              // reset the map 
              map.removeLayer(marker);
              map.removeLayer(circle);

              // Reset the continue button and the result
              $('#continue').addClass('disabled');
              $('.answer').html('Your answer shows in here');
            });
          }
          
          // GAME OVER
          else {
            
            // Message if there was only one correct answer
            if (sessionStorage.getItem('yg_correct') === '1') {
              // let them know it's game over
            window.alert('And that\'s GAME OVER.\n\nThis time you got 1 answer right\n\nYou\'ll do better next time, I\'m sure!\n\nBest of luck in your travels...');
            } 
            else {
              window.alert('And that\'s GAME OVER.\n\nThis time you got ' + sessionStorage.getItem('yg_correct') + ' answers right\n\nYou\'ll do better next time, I\'m sure!\n\nBest of luck in your travels...');
            }

            // send user back to home page
            window.location.replace(window.location.pathname);
          }
        }
      };
    }

    /* 
     * Function checkIfEnd
     * checks if the player got to the end of the game
     * and shows the result if true
     */
    function checkIfEnd() {
      
      // check if the user has gone through all of the cities
      if(parseInt(sessionStorage.getItem('yg_count')) > 8) {
       
        // grab the number of correct answers in a variable
        var correctAnswers = parseInt(sessionStorage.getItem('yg_correct'));

        // different messages depending on number of correct answers
        if (correctAnswers === 1) {
          window.alert("Well done! You made it!\n\nYou got " + sessionStorage.getItem('yg_correct') + " answer right");        
        }

        else if (correctAnswers >1 && correctAnswers < 8 ) {
          window.alert("Well done! You made it!\n\nYou got " + sessionStorage.getItem('yg_correct') + " answers right");        
        }

        else if (correctAnswers === 8) {
          window.alert("Well, aren't you a seasoned traveller! \n\nYou didn't make any mistakes! Well done you!"); 
        }

        window.location.replace(window.location.pathname);
      }
    }
  }]);